FROM alpine:3.16.2
ARG GRYPE_VERSION="0.52.0"
ENV URL "https://raw.githubusercontent.com/anchore/grype/main/install.sh"
RUN apk --no-cache add ca-certificates && \
    wget -O - $URL | sh -s -- -b /usr/local/bin v${GRYPE_VERSION}
COPY .grype.yaml /root
COPY cache /root/cache
WORKDIR /root 
ENTRYPOINT ["/usr/local/bin/grype"]
