[![linuxserver.io](https://stef33560.gitlab.io/img/avatar.png)](https://stef33560.gitlab.io/docs/docker/outillage/#action- "That's here I'm talking about it")

![](https://static.scarf.sh/a.png?x-pxid=ef64229d-0a21-4f6b-bfd9-bcf482f61e11)
# [docker33560/grype](https://gitlab.com/docker33560/grype)

[![Scarf.io pulls](https://scarf.sh/installs-badge/stef/docker33560%2Fgrype?package-type=docker)](https://scarf.sh/gateway/stef33560/docker/docker33560%2Fgrype)
[![GitLab Stars](https://img.shields.io/gitlab/stars/docker33560/grype.svg?style=for-the-badge&logo=gitlab)](https://gitlab.com/docker33560)
[![GitLab Release](https://img.shields.io/gitlab/v/release/docker33560/grype?style=for-the-badge&logo=gitlab)](https://gitlab.com/docker33560/grype/releases)
[![GitLab Container Registry](https://img.shields.io/static/v1.svg?style=for-the-badge&label=stef33560&message=GitLab%20Registry&logo=gitlab)](https://gitlab.com/docker33560/grype/container_registry)

[anchore/grype](https://github.com/anchore/grype) est un analyseur permettant entre autres de détecter des CVE. Plus d'infos dispo sur [mon site](https://stef33560.gitlab.io/docs/docker/outillage#d%C3%A9tection-des-failles-de-s%C3%A9curit%C3%A9-cve) 

## Usage

Voici un exemple pour créer un conteneur.

```bash
docker run --rm \
  --network="none" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v $PWD:/tmp/
  -v ~/.cache/grype:/ \
  stef.docker.scarf.sh/docker33560/grype:latest
```

Grype n'a pas besoin de réseau sinon pour télécharger la base, d'où le `--network="none"` puisque ce conteneur ayant vocation à s'exécuter en environnement airgapped il embarque déjà une base de vulnérabilités. 

Le format de sortie prédéfini est au format `json`, dans le fichier `/tmp/json` ; `-v $PWD:/tmp/` permet de récupérer ce fichier dans le dossier courant.

Les autres [options](./.grype.yaml) sont les suivantes ;

- pas de check de mise à jour au démarrage
- pas de mise à jour automatique de la base
- âge maximum de la base de vulnérabilités : **1 mois**  
❗️ Ceci fixe la date d'**obscolescence programmée** de ce conteneur si aucune autre base ne lui est fournie à coté (`-v ~/.cache/grype:/`). La commande suivante vous permettra de récupéer la date de build (`NuildDate`) et d'en déduire la date d'obsolescence : `docker run --rm stef.docker.scarf.sh/docker33560/grype:latest db status`

## Build

### local

```bash
git clone https://gitlab.com/docker33560/grype.git stef33560-grype
cd stef33560-grype
docker build \
  --no-cache \
  --pull \
  --build-arg GRYPE_VERSION="0.52.0" #optionnal
  -t stef.docker.scarf.sh/docker33560/grype:latest .
```

### automatique

Adossée à une [Crontab](https://gitlab.com/docker33560/grype/-/pipeline_schedules) exécutée tous les jours à 07.30 GMT+1, ce dépot se met à jour quotidiennement via sa [GitLab CI](./.gitlab-ci.yml) pour rappatrier la base de données de Grype. Cela permet de s'en servir de [mirror](https://git-scm.com/docs/git-clone#Documentation/git-clone.txt---bare) à destination d'un environnement airgapped.

## Versions

- **11.11.22:** - Initial Release
- **15.11.2022:** - 📦 NEW: database autosync
- **2022-11-15T00:45:52Z** - Update 15067cb9

- **2022-11-16T06:32:34Z** - Update 8b3529ac

- **2022-11-17T06:34:32Z** - Update 9e639b24

- **2022-11-18T06:34:42Z** - Update 96f7002a

- **2022-11-25T06:36:14Z** - Update 1dfa8eed

- **2022-12-02T06:37:19Z** - Update d60b3d3c

- **2022-12-09T06:36:32Z** - Update c75e2127

- **2022-12-23T06:37:39Z** - Update d4b51370

- **2022-12-30T06:38:25Z** - Update 0d65dbfa
